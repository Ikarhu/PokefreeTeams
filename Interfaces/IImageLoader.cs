﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsPokefree.Interface
{
    public interface IImageLoader
    {
        string GetPokemonImageUrl(string pokemonName);
        void LoadImageFromUrl(PictureBox pictureBox, string imageUrl);
    }
}