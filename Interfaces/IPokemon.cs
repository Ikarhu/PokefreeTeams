﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsPokefree.Interface
{
    public interface IPokemon
    {
        string Name { get; set; }
        string Type { get; set; }
        string Id { get; set; }
        int DexId { get; set; }
        bool IsShiny { get; set; }
    }
}
