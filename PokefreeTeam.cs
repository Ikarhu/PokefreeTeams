﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MaterialSkin.Controls;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using WindowsFormsPokefree.Class;
using WindowsFormsPokefree.Class.Utils;
using WindowsFormsPokefree.Interface;

namespace WindowsFormsPokefree
{
    public partial class PokefreeTeam : Form
    {
        List<Pokemon> pokemonTeams = new List<Pokemon>();
        private readonly IImageLoader imageLoader = new ImageLoader();
        private bool isDragging = false;
        private Point lastCursorPosition;
        private readonly ApiClient apiClient;

        public PokefreeTeam()
        {
            apiClient = new ApiClient();


            InitializeComponent();
            username.Text = AppSettings.UserName;
            //Load Datas from async func
            LoadDataAsync();
        }

        private void GenerateMaterialCards(List<Pokemon> pokemonList)
        {
            List<MaterialCard> materialCards = new List<MaterialCard>();
            int i = 0;

            foreach (var pokemon in pokemonList)
            {
                MaterialCard card = CreateMaterialCard(pokemon, $"materialCard{i}");
                card.Location = new Point(10, 10);
                pokemonLayoutPanel.Controls.Add(card);
                materialCards.Add(card);
                i++;
            }
        }

        private MaterialCard CreateMaterialCard(Pokemon pokemon, string name)
        {
            MaterialCard card = new MaterialCard
            {
                Depth = 0,
                MouseState = MouseState.HOVER,
                Name = name,
                Padding = new Padding(10),
                Size = new Size(125, 100)
            };
            MaterialLabel nameLabel = new MaterialLabel
            {
                Text = pokemon.Name,
                AutoSize = false,
                Width = card.Width,
                TextAlign = ContentAlignment.MiddleCenter
            };


            PictureBox pictureBox = new PictureBox
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(80, 80),
                Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right
            };

            pictureBox.Location = new Point((card.Width - pictureBox.Width) / 2, (card.Height - pictureBox.Height - nameLabel.Height) / 2);

            string imageUrl = imageLoader.GetPokemonImageUrl(pokemon.Name);

            imageLoader.LoadImageFromUrl(pictureBox, imageUrl);

            card.Controls.Add(nameLabel);
            card.Controls.Add(pictureBox);

            return card;
        }


        private async void LoadDataAsync()
        {
            try
            {
                //Fetch Datas from ApiClient
                await FetchDataAsync();
                //If player has a team we generate material Cards for his team
                if (pokemonTeams != null && pokemonTeams.Count > 0)
                    GenerateMaterialCards(pokemonTeams);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading data: {ex.Message}");
            }
        }

        private async Task FetchDataAsync()
        {
            try
            {
                string response = await apiClient.GetTeamDataAsync();

                pokemonTeams = JsonUtils.Deserialize<Pokemon>(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error fetching data: {ex.Message}");
            }
        }
 
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NextForm_click(object sender, EventArgs e)
        {
            ShowNextForm();
        }

        private void ShowNextForm()
        {
            if (username.Text != "")
            {
                AppSettings.UserName = username.Text;
                SetUsername setUsername = new SetUsername();
                setUsername.StartPosition = FormStartPosition.Manual;
                setUsername.Location = this.Location;
                this.Hide();
                setUsername.Show();
            }
        }

        private void Logo_Click(object sender, EventArgs e)
        {
            List<Control> listControls = new List<Control>();

            foreach (Control control in pokemonLayoutPanel.Controls)
            {
                listControls.Add(control);
            }

            foreach (Control control in listControls)
            {
                pokemonLayoutPanel.Controls.Remove(control);
                control.Dispose();
            }
            if (pokemonTeams != null)
                pokemonTeams.Clear();
            pokemonTeams = new List<Pokemon>();
            LoadDataAsync();
            GenerateMaterialCards(pokemonTeams);
        }

        private void FlowLayoutPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursorPosition = new Point(e.X, e.Y);
            }
        }

        private void FlowLayoutPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                int deltaX = e.X - lastCursorPosition.X;
                int deltaY = e.Y - lastCursorPosition.Y;

                this.Location = new Point(this.Location.X + deltaX, this.Location.Y + deltaY);
                lastCursorPosition = new Point(e.X, e.Y);
            }
        }

        private void FlowLayoutPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }
    }
}
