﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsPokefree.Class;

namespace WindowsFormsPokefree
{
    public partial class SetUsername : Form
    {
        private bool isDragging = false;
        private Point lastCursorPosition;

        public SetUsername()
        {
            InitializeComponent();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FlowLayoutPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastCursorPosition = new Point(e.X, e.Y);
            }
        }

        private void FlowLayoutPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                int deltaX = e.X - lastCursorPosition.X;
                int deltaY = e.Y - lastCursorPosition.Y;

                this.Location = new Point(this.Location.X + deltaX, this.Location.Y + deltaY);
                lastCursorPosition = new Point(e.X, e.Y);
            }
        }

        private void FlowLayoutPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;
            }
        }

        private void Username_Key_Press(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            ShowNextForm();
        }

        private void NextForm_click(object sender, EventArgs e)
        {
            ShowNextForm();
        }

        private void ShowNextForm()
        {
            if (username.Text != "")
            {
                AppSettings.UserName = username.Text;
                PokefreeTeam team = new PokefreeTeam();
                team.StartPosition = FormStartPosition.Manual;
                team.Location = this.Location;
                this.Hide();
                team.Show();
            }
        }
    }
}
