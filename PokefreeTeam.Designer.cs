﻿using MaterialSkin.Controls;

namespace WindowsFormsPokefree
{
    partial class PokefreeTeam
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pokemonLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.username = new MaterialSkin.Controls.MaterialLabel();
            this.OptionsMenu = new System.Windows.Forms.FlowLayoutPanel();
            this.closeButton = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.backButton = new System.Windows.Forms.PictureBox();
            this.logo = new System.Windows.Forms.PictureBox();
            this.pokemonLayoutPanel.SuspendLayout();
            this.OptionsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // pokemonLayoutPanel
            // 
            this.pokemonLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.pokemonLayoutPanel.Controls.Add(this.label1);
            this.pokemonLayoutPanel.Location = new System.Drawing.Point(12, 119);
            this.pokemonLayoutPanel.Name = "pokemonLayoutPanel";
            this.pokemonLayoutPanel.Size = new System.Drawing.Size(320, 420);
            this.pokemonLayoutPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Depth = 0;
            this.username.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.username.Location = new System.Drawing.Point(74, 63);
            this.username.MouseState = MaterialSkin.MouseState.HOVER;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(118, 19);
            this.username.TabIndex = 1;
            this.username.Text = "Pixelmon_Ikarhu";
            // 
            // OptionsMenu
            // 
            this.OptionsMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(62)))));
            this.OptionsMenu.Controls.Add(this.closeButton);
            this.OptionsMenu.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.OptionsMenu.Location = new System.Drawing.Point(45, 0);
            this.OptionsMenu.Name = "OptionsMenu";
            this.OptionsMenu.Size = new System.Drawing.Size(300, 30);
            this.OptionsMenu.TabIndex = 3;
            this.OptionsMenu.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseDown);
            this.OptionsMenu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseMove);
            this.OptionsMenu.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseUp);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Transparent;
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.Image = global::WindowsFormsPokefree.Properties.Resources.circle;
            this.closeButton.Location = new System.Drawing.Point(273, 3);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(24, 24);
            this.closeButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.closeButton.TabIndex = 5;
            this.closeButton.TabStop = false;
            this.closeButton.Click += new System.EventHandler(this.Close_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(62)))));
            this.flowLayoutPanel1.Controls.Add(this.backButton);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(45, 30);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.Color.Transparent;
            this.backButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.backButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backButton.Image = global::WindowsFormsPokefree.Properties.Resources.left_arrow;
            this.backButton.Location = new System.Drawing.Point(3, 3);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(24, 24);
            this.backButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.backButton.TabIndex = 5;
            this.backButton.TabStop = false;
            this.backButton.Click += new System.EventHandler(this.NextForm_click);
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.logo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logo.Image = global::WindowsFormsPokefree.Properties.Resources.logo_2;
            this.logo.Location = new System.Drawing.Point(18, 48);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(50, 50);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo.TabIndex = 2;
            this.logo.TabStop = false;
            this.logo.Click += new System.EventHandler(this.Logo_Click);
            // 
            // PokefreeTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(345, 550);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.OptionsMenu);
            this.Controls.Add(this.username);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.pokemonLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PokefreeTeam";
            this.Text = "Form1";
            this.pokemonLayoutPanel.ResumeLayout(false);
            this.pokemonLayoutPanel.PerformLayout();
            this.OptionsMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.backButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel pokemonLayoutPanel;
        private System.Windows.Forms.Label label1;
        private MaterialLabel username;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.FlowLayoutPanel OptionsMenu;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox closeButton;
        private System.Windows.Forms.PictureBox backButton;
    }
}

