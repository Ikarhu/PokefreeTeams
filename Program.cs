﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsPokefree.Class;
using Microsoft.Extensions.DependencyInjection;
using WindowsFormsPokefree.Class.Utils;
using MaterialSkin;

namespace WindowsFormsPokefree
{
    internal static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Black Theme
            MaterialSkinManager.Instance.Theme = MaterialSkinManager.Themes.DARK;
            //Override with Pokefree Theme
            MaterialSkinManager.Instance.ColorScheme = new ColorScheme(Primary.Yellow700, Primary.Blue700, Primary.Blue700, Accent.Yellow700, TextShade.WHITE);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SetUsername());
        }
    }
}
