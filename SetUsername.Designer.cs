﻿namespace WindowsFormsPokefree
{
    partial class SetUsername
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OptionsMenu = new System.Windows.Forms.FlowLayoutPanel();
            this.username = new MaterialSkin.Controls.MaterialTextBox2();
            this.seeButton = new MaterialSkin.Controls.MaterialButton();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.close = new System.Windows.Forms.PictureBox();
            this.logo = new System.Windows.Forms.PictureBox();
            this.OptionsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // OptionsMenu
            // 
            this.OptionsMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OptionsMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(62)))));
            this.OptionsMenu.Controls.Add(this.close);
            this.OptionsMenu.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.OptionsMenu.Location = new System.Drawing.Point(0, 0);
            this.OptionsMenu.Name = "OptionsMenu";
            this.OptionsMenu.Size = new System.Drawing.Size(345, 30);
            this.OptionsMenu.TabIndex = 4;
            this.OptionsMenu.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseDown);
            this.OptionsMenu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseMove);
            this.OptionsMenu.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FlowLayoutPanel_MouseUp);
            // 
            // username
            // 
            this.username.AnimateReadOnly = false;
            this.username.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.username.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.username.Depth = 0;
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.username.HideSelection = true;
            this.username.LeadingIcon = null;
            this.username.Location = new System.Drawing.Point(80, 285);
            this.username.MaxLength = 32767;
            this.username.MouseState = MaterialSkin.MouseState.OUT;
            this.username.Name = "username";
            this.username.PasswordChar = '\0';
            this.username.PrefixSuffixText = null;
            this.username.ReadOnly = false;
            this.username.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.username.SelectedText = "";
            this.username.SelectionLength = 0;
            this.username.SelectionStart = 0;
            this.username.ShortcutsEnabled = true;
            this.username.Size = new System.Drawing.Size(180, 48);
            this.username.TabIndex = 6;
            this.username.TabStop = false;
            this.username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.username.TrailingIcon = null;
            this.username.UseSystemPasswordChar = false;
            this.username.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Username_Key_Press);
            // 
            // seeButton
            // 
            this.seeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.seeButton.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.seeButton.Depth = 0;
            this.seeButton.HighEmphasis = true;
            this.seeButton.Icon = null;
            this.seeButton.Location = new System.Drawing.Point(132, 342);
            this.seeButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.seeButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.seeButton.Name = "seeButton";
            this.seeButton.NoAccentTextColor = System.Drawing.Color.Empty;
            this.seeButton.Size = new System.Drawing.Size(64, 36);
            this.seeButton.TabIndex = 7;
            this.seeButton.Text = "Voir";
            this.seeButton.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.seeButton.UseAccentColor = false;
            this.seeButton.UseVisualStyleBackColor = true;
            this.seeButton.Click += new System.EventHandler(this.NextForm_click);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.Location = new System.Drawing.Point(142, 263);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(54, 19);
            this.materialLabel1.TabIndex = 8;
            this.materialLabel1.Text = "Pseudo";
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.Image = global::WindowsFormsPokefree.Properties.Resources.circle;
            this.close.Location = new System.Drawing.Point(318, 3);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(24, 24);
            this.close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.close.TabIndex = 4;
            this.close.TabStop = false;
            this.close.Click += new System.EventHandler(this.Close_Click);
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.logo.Image = global::WindowsFormsPokefree.Properties.Resources.logo_2;
            this.logo.Location = new System.Drawing.Point(80, 80);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(180, 164);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo.TabIndex = 5;
            this.logo.TabStop = false;
            // 
            // SetUsername
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(345, 500);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.seeButton);
            this.Controls.Add(this.username);
            this.Controls.Add(this.OptionsMenu);
            this.Controls.Add(this.logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SetUsername";
            this.Text = "SetUsername";
            this.OptionsMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel OptionsMenu;
        private System.Windows.Forms.PictureBox close;
        private System.Windows.Forms.PictureBox logo;
        private MaterialSkin.Controls.MaterialTextBox2 username;
        private MaterialSkin.Controls.MaterialButton seeButton;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
    }
}