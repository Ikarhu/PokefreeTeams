﻿using WindowsFormsPokefree.Interface;

namespace WindowsFormsPokefree.Class
{
    public class Pokemon : IPokemon
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public int DexId { get; set; }
        public bool IsShiny { get; set; }
    }
}
