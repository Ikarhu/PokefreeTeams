﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsPokefree.Class.Utils;

namespace WindowsFormsPokefree.Class
{
    public class ApiClient
    {
        private readonly string apiUrl;
        private readonly HttpClient httpClient;
        private string username = "";

        public ApiClient()
        {
            this.apiUrl = Constants.ApiUrl;
            this.httpClient = new HttpClient();
            if (AppSettings.UserName != null )
                username = AppSettings.UserName; 
        }

        public async Task<string> GetTeamDataAsync()
        {
            string result = null;

            try
            {
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("username", username);

                HttpResponseMessage response = await httpClient.GetAsync($"{apiUrl}/getTeam");

                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }

            return result;
        }
    }
}
