﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace WindowsFormsPokefree.Class.Utils
{
    public static class JsonUtils
    {
        public static List<T> Deserialize<T>(string jsonData)
        {
            try
            {
                List<T> result = JsonConvert.DeserializeObject<List<T>>(jsonData);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deserializing JSON: {ex.Message}");
                return null;
            }
        }
    }
}
