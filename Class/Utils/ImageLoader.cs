﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WindowsFormsPokefree.Interface;

namespace WindowsFormsPokefree.Class.Utils
{
    public class ImageLoader : IImageLoader
    {
        public string GetPokemonImageUrl(string pokemonName)
        {
            return $"{Constants.ImageUrl}{pokemonName.ToLower()}.png";
        }

        public void LoadImageFromUrl(PictureBox pictureBox, string imageUrl)
        {
            try
            {
                pictureBox.Image = Image.FromStream(new System.Net.WebClient().OpenRead(imageUrl));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading image: {ex.Message}");
            }
        }
    }
}
